# Reproductor Genesis

## Tecnologias usadas [PyQt 5], [Python 3], [Pyqt5 Tools]

[PyQt 5]: https://www.riverbankcomputing.com/software/pyqt/download5

[Python 3]: https://www.python.org/ftp/python/3.8.2/python-3.8.2-amd64.exe

[Pyqt5 Tools]: https://pypi.org/project/pyqt5-tools/


## Funcion para poder Arrastrar y soltar el video al reproductor
## Creada por nuestro colaborador  ***@abeljm***

## ![][4]

[4]: img/4.png

## Indispensable instalar estas tecnologias

## MatroskaSplitter y LAVFilters (Codecs), para que pueda reproducir videos y audio si estas en Windows 10

## [LAVFilters]
[LAVFilters]: https://github.com/Nevcairiel/LAVFilters/releases

## [MatroskaSplitter]
[MatroskaSplitter]: https://haali.su/mkv/

## Video demostrativo

## Gracias a [***@fhernd***] por su aportacion ....

[***@fhernd***]: https://github.com/Fhernd

## Gracias a ***@unihernandez22*** por su aporte boton menu


[![Genesis](img/5.png)](https://vimeo.com/397818478 "Genesis")
